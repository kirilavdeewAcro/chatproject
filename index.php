<?php

require __DIR__ . '/bootstrap/autoload.php';

use React\Socket\ConnectionInterface;
use React\EventLoop\Factory as EventLoopFactory;
use React\Socket\Server;
use App\Controllers\SocketMessageController;
use App\Util\StringUtil;
use App\Services\UserService;
use App\Exceptions\CommandException;
use \App\Util\CommonUtil;

$loop = EventLoopFactory::create();
$socket = new Server('127.0.0.1:8080', $loop);

$socket->on('connection', function(ConnectionInterface $connection){
    $usersCountInChat = UserService::getUsersCount();
    $connection->write(StringUtil::addNewline(CommonUtil::getChatGreetingMessage($usersCountInChat)));
    UserService::addAnonymConnection($connection);

    $connection->on('data', function($data) use ($connection){
        try {
            $messageController = new SocketMessageController($data, $connection);
            $messageController->process();
        } catch (CommandException $commandException){
            $connection->write(StringUtil::addNewline($commandException->getMessage()));
        }
    });

    $connection->on('close',  function() use ($connection){
        UserService::removeUser($connection);
    });
});

echo "Server running on {$socket->getAddress()}";

$loop->run();
