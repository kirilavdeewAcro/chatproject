<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirill
 * Date: 11.09.18
 * Time: 18:54
 */

namespace App\Controllers;

use App\Exceptions\CommandException;
use App\Instances\ConnectionsInstance;
use App\Services\UserService;
use App\Util\Constants;
use App\Util\StringUtil;
use React\Socket\ConnectionInterface;

class SocketMessageController
{
    /** @var string */
    private $message;

    /** @var \React\Socket\ConnectorInterface */
    private $connection;


    public function __construct(string $message, ConnectionInterface $connection)
    {
        $this->message = StringUtil::removeNewlines($message);
        $this->connection = $connection;
    }

    /**
     * @throws CommandException
     */
    public function process() : void
    {
        if ($this->isCommand()){
            $this->processCommand();
            return;
        }
        if(!UserService::isUserHaveEnteredNickname($this->connection)){
            UserService::setNickname($this->connection, $this->message);
            return;
        }
        $connections = ConnectionsInstance::getInstance()->getAll();
        foreach ($connections as $connection){
            if ($connection !== $this->connection){
                $connection->write($this->prepareMessageFromUser());
            }
        }
    }

    /**
     * @throws CommandException
     */
    private function processCommand()
    {
        $message = explode(' ', $this->message);
        switch($message[0]){
            case Constants::QUIT_COMMAND:
                UserService::removeUser($this->connection);
                break;
            case Constants::CHANGE_NICKNAME_COMMAND:
                if(empty($message[1])){
                    throw new CommandException("Nickname cannot be empty");
                }
                UserService::setNickname($this->connection, $message[1]);
                break;
            default:
                throw new CommandException("Sorry, command $message[0] does not support");
        }
    }

    /**
     * @return string
     */
    private function prepareMessageFromUser() : string
    {
        $message = trim(preg_replace('/\s\s+/', ' ', $this->message));
        $nickname = UserService::getNickname($this->connection);
        if($nickname){
            $message = $nickname. '> ' .$message;
        }
        return StringUtil::addNewline($message);
    }


    /**
     * @return bool
     */
    private function isCommand() : bool
    {
        return $this->message[0] === Constants::COMMAND_SYMBOL;
    }
}
