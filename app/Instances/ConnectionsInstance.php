<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirill
 * Date: 11.09.18
 * Time: 18:23
 */
namespace App\Instances;

use React\Socket\ConnectionInterface;
use SplObjectStorage;

class ConnectionsInstance
{
    /** @var SplObjectStorage */
    private $connections;

    /** @var ConnectionsInstance */
    private static $instance;

    private function __construct()
    {
        $this->connections = new SplObjectStorage();
    }

    /**
     * @return ConnectionsInstance
     */
    public static function getInstance()
    {
        if(!self::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param ConnectionInterface $connection
     * @return void
     */
    public function add(ConnectionInterface $connection) : void
    {
        $this->connections->attach($connection);
    }

    /**
     * @param ConnectionInterface $connection
     * @return void
     */
    public function remove(ConnectionInterface $connection) : void
    {
        $this->connections->detach($connection);
    }

    /**
     * @param ConnectionInterface $connection
     * @param $data mixed
     * @return void
     */
    public function setConnectionData(ConnectionInterface $connection, $data) : void
    {
        $this->connections->offsetSet($connection, $data); // handle here exception
    }

    /**
     * @param ConnectionInterface $connection
     * @return mixed
     */
    public function getConnectionData(ConnectionInterface $connection)
    {
        return $this->connections->offsetGet($connection);
    }

    /**
     * @return int
     */
    public function getConnectionsCount() : int
    {
        return $this->connections->count();
    }

    /**
     * @return SplObjectStorage
     */
    public function getAll() : SplObjectStorage
    {
        return $this->connections;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return $this->connections->count();
    }

}
