<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirill
 * Date: 12.09.18
 * Time: 16:50
 */

namespace App\Util;


class StringUtil
{
    /**
     * @param string $text
     * @return string
     */
    public static function addNewline(string $text) : string
    {
        return $text . "\r\n";
    }

    /**
     * @param string $text
     * @return string
     */
    public static function removeNewlines(string $text) : string
    {
        return trim(preg_replace('/\s\s+/', ' ', $text));
    }
}
