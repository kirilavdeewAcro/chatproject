<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirill
 * Date: 12.09.18
 * Time: 12:53
 */

namespace App\Util;


class Constants
{
    const COMMAND_SYMBOL = '/';
    const QUIT_COMMAND = '/quit';
    const CHANGE_NICKNAME_COMMAND = '/nick';
}
