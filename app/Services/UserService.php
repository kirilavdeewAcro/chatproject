<?php
/**
 * Created by IntelliJ IDEA.
 * User: kirill
 * Date: 12.09.18
 * Time: 17:04
 */

namespace App\Services;

use App\Instances\ConnectionsInstance;
use React\Socket\ConnectionInterface;


class UserService
{
    /**
     * @param ConnectionInterface $connection
     * @return bool
     */
    public static function isUserHaveEnteredNickname(ConnectionInterface $connection) : bool
    {
        return !empty(ConnectionsInstance::getInstance()->getConnectionData($connection));
    }

    /**
     * @param ConnectionInterface $connection
     * @param string $nickname
     * @return void
     */
    public static function setNickname(ConnectionInterface $connection, string $nickname) : void
    {
        ConnectionsInstance::getInstance()->setConnectionData($connection, $nickname);
    }

    /**
     * @param ConnectionInterface $connection
     * @return string
     */
    public static function getNickname(ConnectionInterface $connection) : string
    {
        return ConnectionsInstance::getInstance()->getConnectionData($connection);
    }


    /**
     * @param ConnectionInterface $connection
     * @return void
     */
    public static function addAnonymConnection(ConnectionInterface $connection)
    {
        ConnectionsInstance::getInstance()->add($connection);
    }

    /**
     * @param ConnectionInterface $connection
     * @return void
     */
    public static function removeUser(ConnectionInterface $connection) : void
    {
        ConnectionsInstance::getInstance()->remove($connection);
        $connection->close();
    }

    /**
     * @return int
     */
    public static function getUsersCount() : int
    {
        return ConnectionsInstance::getInstance()->count();
    }

    /**
     * @return array
     */
    public static function getAllUsers() : array
    {
        return iterator_to_array(ConnectionsInstance::getInstance()->getAll());
    }
}
