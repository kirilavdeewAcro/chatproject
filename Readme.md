### Requirements
```
    php >= 7.1
    composer
```
### Installation
```bash
    composer install
```
### Running
```bash
php index.php
```
